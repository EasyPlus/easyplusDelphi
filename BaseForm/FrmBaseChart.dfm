object FormBaseChart: TFormBaseChart
  Left = 354
  Top = 104
  Width = 757
  Height = 675
  Caption = 'FormBaseChart'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object dbcht: TDBChart
    Left = 0
    Top = 0
    Width = 749
    Height = 644
    AllowPanning = pmNone
    BackWall.Color = clWhite
    BackWall.Dark3D = False
    BackWall.Gradient.EndColor = 11118482
    BackWall.Size = 8
    BackWall.Transparent = False
    Border.Visible = True
    BottomWall.Dark3D = False
    BottomWall.Gradient.EndColor = 16580349
    BottomWall.Gradient.StartColor = 3114493
    BottomWall.Size = 8
    Foot.Font.Name = 'Verdana'
    LeftWall.Color = clWhite
    LeftWall.Dark3D = False
    LeftWall.Gradient.EndColor = 2413052
    LeftWall.Gradient.StartColor = 900220
    LeftWall.Size = 8
    RightWall.Color = clWhite
    RightWall.Dark3D = False
    RightWall.Size = 8
    SubFoot.Font.Name = 'Verdana'
    SubTitle.Font.Name = 'Verdana'
    Title.Font.Color = clBlack
    Title.Font.Height = -16
    Title.Font.Name = 'Times New Roman'
    Title.Font.Style = [fsBold]
    Title.Text.Strings = (
      'TDBChart')
    BottomAxis.Axis.Width = 1
    BottomAxis.Grid.Color = clBlack
    BottomAxis.Grid.Style = psSolid
    BottomAxis.GridCentered = True
    BottomAxis.LabelsFont.Height = -13
    BottomAxis.LabelsFont.Name = 'Times New Roman'
    BottomAxis.MinorTicks.Color = clBlack
    BottomAxis.MinorTicks.Visible = False
    BottomAxis.Ticks.Color = clBlack
    BottomAxis.TicksInner.Color = 11119017
    BottomAxis.TicksInner.Visible = False
    BottomAxis.Title.Font.Name = 'Times New Roman'
    DepthAxis.Axis.Width = 1
    DepthAxis.Grid.Color = clBlack
    DepthAxis.Grid.Style = psSolid
    DepthAxis.LabelsFont.Height = -13
    DepthAxis.LabelsFont.Name = 'Times New Roman'
    DepthAxis.MinorTicks.Color = clBlack
    DepthAxis.MinorTicks.Visible = False
    DepthAxis.Ticks.Color = clBlack
    DepthAxis.TicksInner.Color = 11119017
    DepthAxis.TicksInner.Visible = False
    DepthAxis.Title.Font.Name = 'Times New Roman'
    DepthTopAxis.Axis.Width = 1
    DepthTopAxis.Grid.Color = clBlack
    DepthTopAxis.Grid.Style = psSolid
    DepthTopAxis.LabelsFont.Height = -13
    DepthTopAxis.LabelsFont.Name = 'Times New Roman'
    DepthTopAxis.MinorTicks.Color = clBlack
    DepthTopAxis.MinorTicks.Visible = False
    DepthTopAxis.Ticks.Color = clBlack
    DepthTopAxis.TicksInner.Color = 11119017
    DepthTopAxis.TicksInner.Visible = False
    DepthTopAxis.Title.Font.Name = 'Times New Roman'
    LeftAxis.Axis.Width = 1
    LeftAxis.Grid.Color = clBlack
    LeftAxis.Grid.Style = psSolid
    LeftAxis.LabelsFont.Height = -13
    LeftAxis.LabelsFont.Name = 'Times New Roman'
    LeftAxis.MinorTicks.Color = clBlack
    LeftAxis.MinorTicks.Visible = False
    LeftAxis.Ticks.Color = clBlack
    LeftAxis.TicksInner.Color = 11119017
    LeftAxis.TicksInner.Visible = False
    LeftAxis.Title.Font.Name = 'Times New Roman'
    Legend.Font.Height = -13
    Legend.Font.Name = 'Times New Roman'
    Legend.Frame.Visible = False
    Legend.Gradient.Direction = gdTopBottom
    Legend.Gradient.EndColor = clYellow
    Legend.Gradient.StartColor = clWhite
    Legend.Shadow.Color = clGray
    Legend.Shadow.HorizSize = 0
    Legend.Shadow.VertSize = 0
    Legend.Symbol.DefaultPen = False
    Legend.Symbol.Pen.Visible = False
    Legend.Transparent = True
    RightAxis.Axis.Width = 1
    RightAxis.Grid.Color = clBlack
    RightAxis.Grid.Style = psSolid
    RightAxis.LabelsFont.Height = -13
    RightAxis.LabelsFont.Name = 'Times New Roman'
    RightAxis.MinorTicks.Color = clBlack
    RightAxis.MinorTicks.Visible = False
    RightAxis.Ticks.Color = clBlack
    RightAxis.TicksInner.Color = 11119017
    RightAxis.TicksInner.Visible = False
    RightAxis.Title.Font.Name = 'Times New Roman'
    Shadow.Color = clBlack
    TopAxis.Axis.Width = 1
    TopAxis.Grid.Color = clBlack
    TopAxis.Grid.Style = psSolid
    TopAxis.LabelsFont.Height = -13
    TopAxis.LabelsFont.Name = 'Times New Roman'
    TopAxis.MinorTicks.Color = clBlack
    TopAxis.MinorTicks.Visible = False
    TopAxis.Ticks.Color = clBlack
    TopAxis.TicksInner.Color = 11119017
    TopAxis.TicksInner.Visible = False
    TopAxis.Title.Font.Name = 'Times New Roman'
    View3D = False
    Zoom.Allow = False
    Align = alClient
    BevelOuter = bvNone
    Color = clWhite
    PopupMenu = pm1
    TabOrder = 0
    PrintMargins = (
      15
      11
      15
      11)
    ColorPaletteIndex = 9
  end
  object qry: TADOQuery
    Connection = CommonModule.conDb
    BeforeOpen = qryBeforeOpen
    AfterOpen = qryAfterOpen
    Parameters = <>
    Left = 88
    Top = 96
  end
  object pm1: TPopupMenu
    Left = 128
    Top = 96
    object N1: TMenuItem
      Caption = #20840#23631
      OnClick = N1Click
    end
    object N2: TMenuItem
      Caption = #20445#23384#20026'BMP'
      OnClick = N2Click
    end
    object N4: TMenuItem
      Caption = #26597#30475#25968#25454#28304
    end
    object N3: TMenuItem
      Caption = #20851#38381
      OnClick = N3Click
    end
  end
  object dlgSave: TSaveDialog
    DefaultExt = '*.BMP'
    Filter = '*.BMP'
    Left = 48
    Top = 96
  end
end
