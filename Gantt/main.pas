unit main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, phGant, ImgList, phGrid,
  phGridTree, phBabGridView, phGanttPrinting, Printers, DB, ADODB,ShellAPI,
  ComCtrls;

type
  TMOCTAGanttForm = class(TForm)
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    G1: TphGant;
    ImageList1: TImageList;
    qry1: TADOQuery;
    qry2: TADOQuery;
    qry3: TADOQuery;
    lbl1: TLabel;
    edtTA001: TEdit;
    lbl2: TLabel;
    dtpb: TDateTimePicker;
    lbl3: TLabel;
    dtpe: TDateTimePicker;
    btnOpen: TButton;
    lbl4: TLabel;
    edtTA006: TEdit;
    lbl5: TLabel;
    edtTA034: TEdit;
    edtTA035: TEdit;
    lbl6: TLabel;
    pnl1: TPanel;
    qryUPMOCTA: TADOQuery;
    btnUPDATE: TButton;
    qryUPSFCTA: TADOQuery;
    edtTA002: TEdit;
    lbl7: TLabel;
    lbl8: TLabel;
    lbl9: TLabel;
    edtTA033: TEdit;
    edtInfo: TEdit;
    lbl10: TLabel;
    edtTA003: TEdit;
    lbl11: TLabel;
    procedure GridInitTreeSubDataList(aGrid: TphGrid;
      aGridTree: TphGridTree;
      aDataEntity_GridTreeNode: TphDataEntity_GridTreeNode);
    procedure G1ChangeDataEntity_GantTime(theGant: TphGant_ComBasic;
      theDataEntity: TphDataEntity_GantTime);
    procedure BitBtn1Click(Sender: TObject);
    procedure G1PrintToHdcNewPage(theGant: TphGant_ComBasic;
      aPageNo: Integer; var pageHeight, pageWidht: Integer; var goahead,
      render: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure btnOpenClick(Sender: TObject);
    procedure btnUPDATEClick(Sender: TObject);
    procedure edtTA001KeyPress(Sender: TObject; var Key: Char);
    procedure qry1BeforeOpen(DataSet: TDataSet);
    procedure qry1AfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  MOCTAGanttForm: TMOCTAGanttForm;

implementation

uses phDataEntityPack, Common_Module;

{$R *.dfm}

procedure TMOCTAGanttForm.GridInitTreeSubDataList(aGrid: TphGrid;
  aGridTree: TphGridTree;
  aDataEntity_GridTreeNode: TphDataEntity_GridTreeNode);
var
  i,n : integer;
  Str:string;
  myt:TphDataEntity_GridTreeNode;
begin
    n:=aDataEntity_GridTreeNode.AxisContentItem.ListIndex;
    if aDataEntity_GridTreeNode.Level=0 then
    begin
        qry3.Close;
        qry3.SQL.Text := 'Select TA001,TA002,TA003,TA004,TA007,TA010,TA011,TA032,CONVERT(DATETIME,TA009) TA009,CONVERT(DATETIME,TA008) TA008 From SFCTA Where TA001+''-''+TA002 = '''
                                + g1.Grid.GetText(g1.Grid.Cell[0,aDataEntity_GridTreeNode.AxisContentItem.ListIndex])
                                + ''' AND TA003 LIKE ''%' + edtTA003.Text
                                + '%'' order by TA003 ';
        qry3.open;
        for i:=1 to qry3.RecordCount do
        begin
            inc(n);
            myt:= g1.GridNode_Add(aDataEntity_GridTreeNode);
            g1.Grid.SetText(g1.Grid.Cell[0,n],qry3.FieldbyName('TA003').asstring+'-'+qry3.FieldbyName('TA004').asstring);
            g1.Grid.SetText(g1.Grid.Cell[1,n],qry3.FieldbyName('TA001').asstring+'-'+qry3.FieldbyName('TA002').asstring);
            with G1.GridNode_AddGantTime(myt,0) do
            begin
                Style:=gtsPipe;
                if i mod 2 = 0 then
                  Color:=clSkyBlue
                else
                  Color := clYellow;
                IF qry3.FieldbyName('TA032').asstring ='N' then
                  CanEdit:=True
                else
                  CanEdit := False;
                TextCollection.AddText(qry3.FieldbyName('TA003').asstring +' / '
                            + Trim(qry3.FieldbyName('TA007').asstring) +' /投:'
                            + CurrToStr(qry3.FieldbyName('TA010').AsCurrency)
                            +' /转:'+CurrToStr(qry3.FieldbyName('TA011').AsCurrency),1,1,3);
                Start:=qry3.FieldbyName('TA008').AsDateTime;
                Stop:=qry3.FieldbyName('TA009').AsDateTime;
                UserIntegerReference:=i;
            end;     
            qry3.Next;

        end;
    end;


end;

procedure TMOCTAGanttForm.G1ChangeDataEntity_GantTime(theGant: TphGant_ComBasic;
  theDataEntity: TphDataEntity_GantTime);
var
  i:integer;
  iLevel,n:integer;
  s1,S2:String;
begin

   //FormatDateTime('YYYY-MM-DD',theDataEntity.Stop)
  //Locate('City;District', VarArrayOf(['台北,大安区']),[]);


 // ShowMessage(FormatDateTime('YYYYMMDD',theDataEntity.Start));     //起始日
 // ShowMessage(FormatDateTime('YYYYMMDD',theDataEntity.Stop));      //结束日
 // ShowMessage(IntToStr(theDataEntity.Row.GridTreeNode.AxisContentItem.ListIndex));   //当前行
 // ShowMessage(g1.Grid.GetText(g1.Grid.Cell[0,theDataEntity.Row.GridTreeNode.AxisContentItem.ListIndex]));
   //  theDataEntity.Stop := StrToDateTime(FormatDateTime('YYYY-MM-DD',theDataEntity.Stop) + ' 23:00:00');
   //  theDataEntity.Start := StrToDateTime(FormatDateTime('YYYY-MM-DD',theDataEntity.Start) + ' 00:00:00');
   iLevel := theDataEntity.Row.GridTreeNode.Level;
   n := theDataEntity.Row.GridTreeNode.AxisContentItem.ListIndex;
   if iLevel = 0 then
   begin
      IF qryUPMOCTA.Locate('TA0012',g1.Grid.GetText(g1.Grid.Cell[0,theDataEntity.Row.GridTreeNode.AxisContentItem.ListIndex]),[loCaseInsensitive]) then
      begin

        qryUPMOCTA.Edit;
        qryUPMOCTA.FieldByName('TA009').AsString := FormatDateTime('YYYYMMDD',theDataEntity.Start);
        qryUPMOCTA.FieldByName('TA010').AsString := FormatDateTime('YYYYMMDD',theDataEntity.Stop);
        qryUPMOCTA.Post;
      end;
   end;

   if iLevel = 1 then
   begin
      //ShowMessage(IntToStr(theDataEntity.Row.GridTreeNode.ParentNode.AxisContentItem.ListIndex));
      s1 := g1.Grid.GetText(g1.Grid.Cell[0,theDataEntity.Row.GridTreeNode.AxisContentItem.ListIndex]);
      S2 :=  g1.Grid.GetText(g1.Grid.Cell[1,theDataEntity.Row.GridTreeNode.AxisContentItem.ListIndex]);
      IF qryUPSFCTA.Locate('TA0034;TA0012',VarArrayOf([S1,S2]),[loCaseInsensitive]) then
      begin

        qryUPSFCTA.Edit;
        qryUPSFCTA.FieldByName('TA008').AsString := FormatDateTime('YYYYMMDD',theDataEntity.Start);
        qryUPSFCTA.FieldByName('TA009').AsString := FormatDateTime('YYYYMMDD',theDataEntity.Stop);
        qryUPSFCTA.Post;
      end;
   end;
    if theDataEntity.Style=gtsLine then
    begin
      for i:=0 to theDataEntity.OwningDataList.Count-1 do
      begin
        if ((theDataEntity.OwningDataList[i] as TphDataEntity_GantTime).Style=gtsImage) and
          ((theDataEntity.OwningDataList[i] as TphDataEntity_GantTime).UserIntegerReference=theDataEntity.UserIntegerReference) then
        begin
          (theDataEntity.OwningDataList[i] as TphDataEntity_GantTime).Start:=theDataEntity.Start;
        end;
      end;

    end;



end;

procedure TMOCTAGanttForm.BitBtn1Click(Sender: TObject);
var
  usedwidth,
  usedHeight:integer;
begin

  Printer.BeginDoc;
  Printer.Handle;
  TphPrintGantt.PrintWithGrid(G1,Printer.Handle,10,10,2,2,G1.TreeWidth,nil,-1,true,g1.Start,g1.Stop,g1.Width-g1.TreeWidth,g1.ScalerHeight,usedwidth,usedHeight,0,0,nil);
  Printer.EndDoc;
end;


procedure TMOCTAGanttForm.G1PrintToHdcNewPage(theGant: TphGant_ComBasic;
  aPageNo: Integer; var pageHeight, pageWidht: Integer; var goahead,
  render: Boolean);
begin
    //Answer# continued

  pageHeight:=Printer.PageHeight;
  pageWidht:=Printer.PageWidth;
  goahead:=true;
  render:=true;
  if aPageNo>1 then
    Printer.NewPage;

end;



procedure TMOCTAGanttForm.FormCreate(Sender: TObject);
begin
    G1.Grid.Columns[0].HeaderText := '工单单别单号';
    G1.Grid.Columns[1].HeaderText := '品号';
    G1.Grid.Columns[0].Width := 120;
    dtpe.DateTime := date();
    dtpb.DateTime := date()-30;
end;

procedure TMOCTAGanttForm.btnOpenClick(Sender: TObject);
var
    i,j,ii,h : integer;
    sSQLWhere:String;
begin

  g1.Grid.GetTree.RootDataEntities.Clear;
  for i:=0 to g1.RowList.Count-1 do
  begin
    for ii:=0 to g1.RowList[i].DataLists.Count-1 do
    begin
      g1.RowList[i].DataLists[ii].Clear;
    end;
  end;
  if edtTA001.Text <> '' then
     sSQLWhere := sSQLWhere + ' AND MOCTA.TA001 = ''' + edtTA001.Text + '''';
  IF edtTA006.Text <> '' then
     sSQLWhere := sSQLWhere + ' AND MOCTA.TA006 = ''' + edtTA006.Text + '''';
  IF edtTA033.Text <> '' then
     sSQLWhere := sSQLWhere + ' AND MOCTA.TA033 LIKE ''%' + edtTA033.Text + '%'' ';

  sSQLWhere := sSQLWhere + ' AND MOCTA.TA003 >=''' + FormatDateTime('YYYYMMDD',dtpb.DateTime)
                            + ''' AND MOCTA.TA003 <= ''' + FormatDateTime('YYYYMMDD',dtpE.DateTime) + ''''
                            + ' AND MOCTA.TA034 LIKE ''%' + edtTA034.Text + '%'' AND MOCTA.TA035 LIKE ''%'
                            + edtTA035.Text + '%'' and MOCTA.TA002 LIKE ''%' + edtTA002.Text + '%''  ';
  qry1.Close;
  qry2.Close;
  qry1.SQL.Text := 'Select TA001,TA002,TA003,TA006,TA015,CONVERT(DATETIME,TA009) TA009,CONVERT(DATETIME,TA010) TA010,'
                    +'TA034,TA035,TA017,TA001+''-''+TA002 TA0012,TA011  '
                    +' From MOCTA Where 1 = 1 ' + sSQLWhere;
  qryUPMOCTA.Close;
  qryUPSFCTA.Close;
  qryUPMOCTA.SQL.Text := 'Select *,TA001+''-''+TA002 TA0012 from MOCTA  Where 1 = 1 ' + sSQLWhere;
  qryUPSFCTA.SQL.Text := 'Select SFCTA.*,SFCTA.TA001+''-''+SFCTA.TA002 TA0012,SFCTA.TA003+''-''+SFCTA.TA004 TA0034 from MOCTA INNER JOIN SFCTA ON MOCTA.TA001 = SFCTA.TA001 AND MOCTA.TA002 = SFCTA.TA002 '
                         + 'Where 1 = 1 '  + sSQLWhere;
  qryUPSFCTA.Open;
  qryUPMOCTA.Open;

  qry1.Open;
  qry2.SQL.Text := 'Select CONVERT(DATETIME,Min(TA009)) TA009,CONVERT(DATETIME,MAX(TA010)) TA010 FROM MOCTA Where TA009 <> '''' ' + sSQLWhere;
  qry2.Open;
  edtInfo.Text := '共有工单数量:' + IntToStr(qry1.RecordCount) + '条,最早开工日为'
              + FormatDateTime('YYYY-MM-DD',qry2.FieldbyName('TA009').AsDateTime)
              +',最晚完工日为:'+ FormatDateTime('YYYY-MM-DD',qry2.FieldbyName('TA010').AsDateTime);
    h:=0;
    g1.DateScaler.Start:=qry2.FieldbyName('TA009').AsDateTime;
    g1.DateScaler.Stop:=qry2.FieldbyName('TA010').AsDateTime;
    for i:=1 to qry1.RecordCount do
    begin
        g1.GridNode_Add(nil);
        g1.Grid.SetText(g1.Grid.Cell[0,i],qry1.FieldbyName('TA001').asstring+'-'+qry1.FieldbyName('TA002').asstring);
        g1.Grid.SetText(g1.Grid.Cell[1,i],qry1.FieldbyName('TA006').asstring);
        for j:=1 to 1 do
        begin
            inc(h);
            with G1.GridNode_AddGantTime(g1.RowList.Rows[i-1].GridTreeNode,0) do
            begin
                Style:=gtsPipe;
                 //Style := gtsUser;
                TextCollection.AddText(qry1.FieldbyName('TA034').asstring +' / '
                            + qry1.FieldbyName('TA035').asstring +' /预:'
                            + CurrToStr(qry1.FieldbyName('TA015').AsCurrency)
                            +' /已:'+CurrToStr(qry1.FieldbyName('TA017').AsCurrency),1,1,3);
                Color:=clRed;
                IF (qry1.FieldbyName('TA011').AsString = 'Y') or (qry1.FieldbyName('TA011').AsString = 'y') then
                  CanEdit:=False
                else
                  CanEdit:=True;
                Start:=qry1.FieldbyName('TA009').AsDateTime;
                Stop:=qry1.FieldbyName('TA010').AsDateTime;
                UserIntegerReference:=j;
            end;
            qry1.Next;
        end;
    end;
end;

procedure TMOCTAGanttForm.btnUPDATEClick(Sender: TObject);
begin
// qryUPMOCTA.ExecSQL;
case 
  Application.MessageBox('确认要将排程数据写入到ERP吗?将会更新工单预计开工,预计完工,工艺预计开工,预计完工!',
  '提示', MB_OKCANCEL + MB_ICONWARNING + MB_DEFBUTTON2) of
  IDOK:
    begin
      qryUPMOCTA.UpdateBatch();
      qryUPSFCTA.UpdateBatch();
    end;
  IDCANCEL:
    begin

    end;
end;


end;

procedure TMOCTAGanttForm.edtTA001KeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then
    btnOpen.Click;
end;

procedure TMOCTAGanttForm.qry1BeforeOpen(DataSet: TDataSet);
begin
  Screen.Cursor := crHourGlass;
end;

procedure TMOCTAGanttForm.qry1AfterOpen(DataSet: TDataSet);
begin
  Screen.Cursor := crDefault;
end;

end.
