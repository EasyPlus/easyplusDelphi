object MOCTAGanttForm: TMOCTAGanttForm
  Left = 345
  Top = 236
  Width = 1159
  Height = 642
  Caption = #24037#21333#24037#33402#25163#24037#25490#31243
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #23435#20307
  Font.Style = []
  OldCreateOrder = False
  WindowState = wsMaximized
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 12
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1151
    Height = 73
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 0
    object lbl1: TLabel
      Left = 8
      Top = 8
      Width = 54
      Height = 12
      Caption = #24037#21333#21333#21035':'
    end
    object lbl2: TLabel
      Left = 384
      Top = 8
      Width = 48
      Height = 12
      Caption = #24320#21333#26085#20174
    end
    object lbl3: TLabel
      Left = 584
      Top = 8
      Width = 48
      Height = 12
      Caption = #24320#21333#26085#21040
    end
    object lbl4: TLabel
      Left = 200
      Top = 32
      Width = 30
      Height = 12
      Caption = #21697#21495':'
    end
    object lbl5: TLabel
      Left = 400
      Top = 33
      Width = 30
      Height = 12
      Caption = #21697#21517':'
    end
    object lbl6: TLabel
      Left = 593
      Top = 33
      Width = 30
      Height = 12
      Caption = #35268#26684':'
    end
    object lbl7: TLabel
      Left = 184
      Top = 8
      Width = 54
      Height = 12
      Caption = #24037#21333#21333#21495':'
    end
    object lbl9: TLabel
      Left = 8
      Top = 32
      Width = 54
      Height = 12
      Caption = #35745#21010#25209#21495':'
    end
    object lbl10: TLabel
      Left = 8
      Top = 56
      Width = 54
      Height = 12
      Caption = #21152#24037#39034#24207':'
    end
    object lbl11: TLabel
      Left = 888
      Top = 56
      Width = 210
      Height = 12
      Caption = #26102#38388#36724#19978#25302#21160#20197#25918#22823#32553#23567','#24038#21491#31661#22836#31227#21160
    end
    object BitBtn1: TBitBtn
      Left = 888
      Top = 5
      Width = 81
      Height = 22
      Caption = #25171#21360
      TabOrder = 4
      OnClick = BitBtn1Click
    end
    object edtTA001: TEdit
      Left = 72
      Top = 5
      Width = 105
      Height = 18
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 3
      OnKeyPress = edtTA001KeyPress
    end
    object dtpb: TDateTimePicker
      Left = 448
      Top = 3
      Width = 121
      Height = 20
      Date = 40752.442619814820000000
      Time = 40752.442619814820000000
      TabOrder = 0
    end
    object dtpe: TDateTimePicker
      Left = 648
      Top = 3
      Width = 121
      Height = 20
      Date = 40752.442619814820000000
      Time = 40752.442619814820000000
      TabOrder = 1
    end
    object btnOpen: TButton
      Left = 784
      Top = 3
      Width = 97
      Height = 25
      Caption = #29983#25104#29976#29305#25490#31243#22270
      TabOrder = 2
      OnClick = btnOpenClick
    end
    object edtTA006: TEdit
      Left = 256
      Top = 29
      Width = 121
      Height = 18
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 7
      OnKeyPress = edtTA001KeyPress
    end
    object edtTA034: TEdit
      Left = 448
      Top = 29
      Width = 121
      Height = 18
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 8
      OnKeyPress = edtTA001KeyPress
    end
    object edtTA035: TEdit
      Left = 646
      Top = 30
      Width = 121
      Height = 18
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 9
      OnKeyPress = edtTA001KeyPress
    end
    object btnUPDATE: TButton
      Left = 784
      Top = 34
      Width = 97
      Height = 25
      Caption = #26356#26032#25490#31243#25968#25454
      TabOrder = 5
      OnClick = btnUPDATEClick
    end
    object edtTA002: TEdit
      Left = 256
      Top = 5
      Width = 121
      Height = 18
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 6
      OnKeyPress = edtTA001KeyPress
    end
    object edtTA033: TEdit
      Left = 72
      Top = 29
      Width = 105
      Height = 18
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 10
      OnKeyPress = edtTA001KeyPress
    end
    object edtInfo: TEdit
      Left = 200
      Top = 51
      Width = 569
      Height = 20
      Color = clScrollBar
      ReadOnly = True
      TabOrder = 11
    end
    object edtTA003: TEdit
      Left = 72
      Top = 53
      Width = 105
      Height = 18
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 12
      OnKeyPress = edtTA001KeyPress
    end
  end
  object G1: TphGant
    Left = 0
    Top = 73
    Width = 1151
    Height = 512
    Hint = #28857#20987#25289#21160
    TreeView.Left = 0
    TreeView.Top = 25
    TreeView.Width = 190
    TreeView.Height = 485
    TreeView.Hint = #28857#20987'+'#21495#23637#24320#24037#24207#20449#24687
    TreeView.AutoExpandLevels = 0
    TreeView.MaxLevels = -1
    TreeView.MultiSelect = True
    TreeView.ScrollBarVert = False
    TreeView.ScrollBarHorz = False
    TreeView.RowSelect = True
    TreeView.ShowHint = True
    Grid.Left = 0
    Grid.Top = 25
    Grid.Width = 190
    Grid.Height = 485
    Grid.Hint = '1111111'
    Grid.Align = alClient
    Grid.BevelOuter = bvNone
    Grid.BorderStyle = bsSingle
    Grid.Color = clWindow
    Grid.Ctl3D = True
    Grid.ParentCtl3D = False
    Grid.TabOrder = 1
    Grid.TabStop = True
    Grid.HasInfoCol = False
    Grid.Columns = <
      item
        Width = 85
        Kind = tckSimpleEdit
        HeaderText = 'Col1'
        SortEnable = True
        ReadOnly = True
        TreeButton = True
        HasIcon = False
        Hidden = False
        Groupcells = False
        NumericSort = False
      end
      item
        Width = 90
        Kind = tckSimpleEdit
        HeaderText = 'Col2'
        SortEnable = False
        ReadOnly = True
        TreeButton = False
        HasIcon = False
        Hidden = False
        Groupcells = False
        NumericSort = False
      end>
    Grid.SubGrids = <
      item
        Columns = <
          item
            Width = 0
            Kind = tckSimpleEdit
            SortEnable = False
            ReadOnly = False
            TreeButton = False
            HasIcon = False
            Hidden = False
            Groupcells = False
            NumericSort = False
          end
          item
            Width = 0
            Kind = tckSimpleEdit
            SortEnable = False
            ReadOnly = False
            TreeButton = False
            HasIcon = False
            Hidden = False
            Groupcells = False
            NumericSort = False
          end
          item
            Width = 0
            Kind = tckSimpleEdit
            SortEnable = False
            ReadOnly = False
            TreeButton = False
            HasIcon = False
            Hidden = False
            Groupcells = False
            NumericSort = False
          end
          item
            Width = 0
            Kind = tckSimpleEdit
            SortEnable = False
            ReadOnly = False
            TreeButton = False
            HasIcon = False
            Hidden = False
            Groupcells = False
            NumericSort = False
          end
          item
            Width = 0
            Kind = tckSimpleEdit
            SortEnable = False
            ReadOnly = False
            TreeButton = False
            HasIcon = False
            Hidden = False
            Groupcells = False
            NumericSort = False
          end
          item
            Width = 0
            Kind = tckSimpleEdit
            SortEnable = False
            ReadOnly = False
            TreeButton = False
            HasIcon = False
            Hidden = False
            Groupcells = False
            NumericSort = False
          end
          item
            Width = 0
            Kind = tckSimpleEdit
            SortEnable = False
            ReadOnly = False
            TreeButton = False
            HasIcon = False
            Hidden = False
            Groupcells = False
            NumericSort = False
          end
          item
            Width = 0
            Kind = tckSimpleEdit
            SortEnable = False
            ReadOnly = False
            TreeButton = False
            HasIcon = False
            Hidden = False
            Groupcells = False
            NumericSort = False
          end
          item
            Width = 0
            Kind = tckSimpleEdit
            SortEnable = False
            ReadOnly = False
            TreeButton = False
            HasIcon = False
            Hidden = False
            Groupcells = False
            NumericSort = False
          end
          item
            Width = 0
            Kind = tckSimpleEdit
            SortEnable = False
            ReadOnly = False
            TreeButton = False
            HasIcon = False
            Hidden = False
            Groupcells = False
            NumericSort = False
          end
          item
            Width = 0
            Kind = tckSimpleEdit
            SortEnable = False
            ReadOnly = False
            TreeButton = False
            HasIcon = False
            Hidden = False
            Groupcells = False
            NumericSort = False
          end>
        HasHeaders = False
        HasInfoCol = False
        ExtendLastColumnToWidth = False
      end>
    Grid.CellLayouts = <
      item
        Name = 'HeaderLayout'
        BackColor = 13303807
        FrontColor = clBlack
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        MinWidth = 20
        MinHeight = 20
        WrapTextAndFit = False
        FrameOptionsUse = False
        FrameOptions = [foRight]
      end
      item
        Name = 'NormalLayout'
        BackColor = clWindow
        FrontColor = clBlack
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        MinWidth = 20
        MinHeight = 20
        WrapTextAndFit = False
        FrameOptionsUse = False
        FrameOptions = [foRight]
      end>
    Grid.RowSelect = True
    Grid.ImageList = ImageList1
    Grid.ExtendLastColumnToWidth = True
    Grid.UserSortEnable = False
    Grid.MultiSelect = False
    Grid.AutoEditMode = False
    Grid.OnInitTreeSubDataList = GridInitTreeSubDataList
    GA.Left = 0
    GA.Top = 45
    GA.Width = 940
    GA.Height = 465
    GA.Hint = '111111111'
    GA.Align = alClient
    GA.TabOrder = 1
    GA.TabStop = True
    DateScaler.Left = 0
    DateScaler.Top = 0
    DateScaler.Width = 940
    DateScaler.Height = 45
    DateScaler.Align = alTop
    DateScaler.ParentShowHint = False
    DateScaler.ShowHint = True
    DateScaler.TabOrder = 0
    DateScaler.TabStop = True
    DateScaler.DateFormat = 'YY/MM/DD'
    DateScaler.TimeFormat = 'HH:MM:SS'
    DateScaler.FirstDayOfWeek = 1
    DateScaler.WeekNumbers = False
    DateScaler.WeekPrefix = ' w '
    DateScaler.WeekYearFormat = 'YYYY'
    DateScaler.QuarterPrefix = ' Q'
    DateScaler.HiddenHours = []
    DateScaler.HiddenDays = []
    DateScaler.IndicatorOn = False
    DateScaler.Horizontal = True
    DateScaler.PixelsForModeSwitch = 15
    DateScaler.Start = 36526.250000000000000000
    DateScaler.Stop = 36526.250000000000000000
    DateScaler.MaxStop = 767011.000000000000000000
    DateScaler.ZoomFactor = 1.000000000000000000
    DateScaler.IndicatorStyle = isNormal
    DateScaler.RescaleWithCtrl = False
    DateScaler.TextFont1.Charset = ANSI_CHARSET
    DateScaler.TextFont1.Color = clWindowText
    DateScaler.TextFont1.Height = -12
    DateScaler.TextFont1.Name = #23435#20307
    DateScaler.TextFont1.Style = []
    DateScaler.TextFont2.Charset = ANSI_CHARSET
    DateScaler.TextFont2.Color = clWindowText
    DateScaler.TextFont2.Height = -14
    DateScaler.TextFont2.Name = #23435#20307
    DateScaler.TextFont2.Style = []
    DateScaler.TextFont3.Charset = ANSI_CHARSET
    DateScaler.TextFont3.Color = clWindowText
    DateScaler.TextFont3.Height = -14
    DateScaler.TextFont3.Name = #23435#20307
    DateScaler.TextFont3.Style = []
    DateScaler.LookAndFeel = tlfBoxed
    DateScaler.ScrollButtons = tsbSmall
    UseGridNotTree = True
    ColorTree = clWindow
    DragCursorTree = -12
    DragModeTree = dmManual
    FontTree.Charset = DEFAULT_CHARSET
    FontTree.Color = clWindowText
    FontTree.Height = -16
    FontTree.Name = 'MS Sans Serif'
    FontTree.Style = []
    BorderStyleTree = bsSingle
    HideSelectionTree = False
    HotTrackTree = False
    IndentTree = 19
    ToolTipsTree = True
    AutoExpandLevelsTree = 0
    MaxLevelsTree = -1
    RightClickSelectTree = False
    MultiSelectTree = True
    AutoExpandTree = False
    RowSelectTree = True
    ShowButtonsTree = True
    ShowLinesTree = True
    CursorTree = 0
    DateFormat = 'YY/MM/DD'
    IndicatorOn = False
    PixelsForModeSwitch = 15
    Start = 36526.250000000000000000
    Stop = 36526.250000000000000000
    TimeFormat = 'HH:MM:SS'
    ZoomFactor = 1.000000000000000000
    ColorScaler = clBtnFace
    FontScaler.Charset = ANSI_CHARSET
    FontScaler.Color = clWindowText
    FontScaler.Height = -12
    FontScaler.Name = #23435#20307
    FontScaler.Style = []
    CursorScaler = 0
    FirstDayOfWeekScaler = 1
    GantBackColor = 16762705
    GantPyjamasColor = 11008762
    MoveInTimeWhenMoveRow = True
    MoveOver12Then24 = True
    DrawShortLines = True
    DrawLongLines = False
    DefaultLinkStyle = tlsMSProject
    ScalerHeight = 45
    LinkShow = lsOneEndVisible
    TreeWidth = 190
    RescaleWithCtrl = False
    StartDragWhenTimeMoveOutside = False
    FontTimeItemStyle1.Charset = DEFAULT_CHARSET
    FontTimeItemStyle1.Color = clWindowText
    FontTimeItemStyle1.Height = -16
    FontTimeItemStyle1.Name = 'MS Sans Serif'
    FontTimeItemStyle1.Style = []
    FontTimeItemStyle2.Charset = DEFAULT_CHARSET
    FontTimeItemStyle2.Color = clWindowText
    FontTimeItemStyle2.Height = -11
    FontTimeItemStyle2.Name = 'MS Sans Serif'
    FontTimeItemStyle2.Style = []
    FontTimeItemStyle3.Charset = DEFAULT_CHARSET
    FontTimeItemStyle3.Color = clWindowText
    FontTimeItemStyle3.Height = -11
    FontTimeItemStyle3.Name = 'MS Sans Serif'
    FontTimeItemStyle3.Style = []
    ImageList = ImageList1
    Grid_CellLayouts = <
      item
        Name = 'HeaderLayout'
        BackColor = 13303807
        FrontColor = clBlack
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        MinWidth = 20
        MinHeight = 20
        WrapTextAndFit = False
        FrameOptionsUse = False
        FrameOptions = [foRight]
      end
      item
        Name = 'NormalLayout'
        BackColor = clWindow
        FrontColor = clBlack
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        MinWidth = 20
        MinHeight = 20
        WrapTextAndFit = False
        FrameOptionsUse = False
        FrameOptions = [foRight]
      end>
    Grid_Columns = <
      item
        Width = 85
        Kind = tckSimpleEdit
        HeaderText = 'Col1'
        SortEnable = True
        ReadOnly = True
        TreeButton = True
        HasIcon = False
        Hidden = False
        Groupcells = False
        NumericSort = False
      end
      item
        Width = 90
        Kind = tckSimpleEdit
        HeaderText = 'Col2'
        SortEnable = False
        ReadOnly = True
        TreeButton = False
        HasIcon = False
        Hidden = False
        Groupcells = False
        NumericSort = False
      end>
    Grid_HasHeaders = True
    Grid_HasInfoCol = False
    Grid_SubGrids = <
      item
        Columns = <
          item
            Width = 0
            Kind = tckSimpleEdit
            SortEnable = False
            ReadOnly = False
            TreeButton = False
            HasIcon = False
            Hidden = False
            Groupcells = False
            NumericSort = False
          end
          item
            Width = 0
            Kind = tckSimpleEdit
            SortEnable = False
            ReadOnly = False
            TreeButton = False
            HasIcon = False
            Hidden = False
            Groupcells = False
            NumericSort = False
          end
          item
            Width = 0
            Kind = tckSimpleEdit
            SortEnable = False
            ReadOnly = False
            TreeButton = False
            HasIcon = False
            Hidden = False
            Groupcells = False
            NumericSort = False
          end
          item
            Width = 0
            Kind = tckSimpleEdit
            SortEnable = False
            ReadOnly = False
            TreeButton = False
            HasIcon = False
            Hidden = False
            Groupcells = False
            NumericSort = False
          end
          item
            Width = 0
            Kind = tckSimpleEdit
            SortEnable = False
            ReadOnly = False
            TreeButton = False
            HasIcon = False
            Hidden = False
            Groupcells = False
            NumericSort = False
          end
          item
            Width = 0
            Kind = tckSimpleEdit
            SortEnable = False
            ReadOnly = False
            TreeButton = False
            HasIcon = False
            Hidden = False
            Groupcells = False
            NumericSort = False
          end
          item
            Width = 0
            Kind = tckSimpleEdit
            SortEnable = False
            ReadOnly = False
            TreeButton = False
            HasIcon = False
            Hidden = False
            Groupcells = False
            NumericSort = False
          end
          item
            Width = 0
            Kind = tckSimpleEdit
            SortEnable = False
            ReadOnly = False
            TreeButton = False
            HasIcon = False
            Hidden = False
            Groupcells = False
            NumericSort = False
          end
          item
            Width = 0
            Kind = tckSimpleEdit
            SortEnable = False
            ReadOnly = False
            TreeButton = False
            HasIcon = False
            Hidden = False
            Groupcells = False
            NumericSort = False
          end
          item
            Width = 0
            Kind = tckSimpleEdit
            SortEnable = False
            ReadOnly = False
            TreeButton = False
            HasIcon = False
            Hidden = False
            Groupcells = False
            NumericSort = False
          end
          item
            Width = 0
            Kind = tckSimpleEdit
            SortEnable = False
            ReadOnly = False
            TreeButton = False
            HasIcon = False
            Hidden = False
            Groupcells = False
            NumericSort = False
          end>
        HasHeaders = False
        HasInfoCol = False
        ExtendLastColumnToWidth = False
      end>
    Grid_RowSelect = True
    Grid_ExtendLastColumnToWidth = True
    Grid_UserSortEnable = False
    Grid_MultiSelect = False
    LookAndFeel = tlfBoxed
    ScrollButtons = tsbSmall
    MaxStop = 767011.000000000000000000
    HiddenHours = []
    HiddenDays = []
    OnPrintToHdcNewPage = G1PrintToHdcNewPage
    TodayLineColor = clBlack
    TodayLineOnOff = False
    BevelOuter = bvLowered
    Align = alClient
    OnChangeDataEntity_GantTime = G1ChangeDataEntity_GantTime
  end
  object pnl1: TPanel
    Left = 0
    Top = 585
    Width = 1151
    Height = 26
    Align = alBottom
    TabOrder = 2
    object lbl8: TLabel
      Left = 16
      Top = 8
      Width = 972
      Height = 12
      Caption = 
        #39118#38505#25552#31034':'#28857#26356#26032#25490#31243#25968#25454#21518','#23558#20250#23545#26377#25913#21160#30340#24037#21333#39044#35745#24320#24037#26085','#39044#35745#23436#24037#26085','#24037#24207#39044#35745#24320#24037#26085','#39044#35745#23436#24037#26085#36827#34892#26356#26032'.'#26412#29256#26412#20026#21151#33021#27979#35797#29256','#26410 +
        #20570#20219#20309#36923#36753#22788#29702','#24050#23436#24037#30340#24037#21333#25110#24037#33402#19981#33021#25302#21160
      Font.Charset = ANSI_CHARSET
      Font.Color = clHotLight
      Font.Height = -12
      Font.Name = #23435#20307
      Font.Style = []
      ParentFont = False
    end
  end
  object ImageList1: TImageList
    Left = 123
    Top = 78
    Bitmap = {
      494C010103000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000007373730073737300737373000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008C8C8C009C9C9C009C9C9C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000007373730084848400848484000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00007B737300ADADAD00E7E7E700C6C6C60073737300737373006B6B6B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000948C8C00BDBDBD00EFEFEF00D6D6D6009C9C9C009C9C9C009C9C9C000000
      0000000000000000000000000000000000000000000000000000000000000000
      000084848400ADADAD00E7E7E700C6C6C6008484840084848400848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000007B7B
      7B00B5B5B500FFFFFF00FFFFFF00E7E7E700E7E7E700B5A59C007B524A00735A
      5A00737373007373730000000000000000000000000000000000000000009494
      9400C6C6C600FFFFFF00FFFFFF00EFEFEF00F7EFEF008CB5C600106B94003973
      8C007B8C8C009C9C9C0000000000000000000000000000000000000000008484
      8400B5B5B500FFFFFF00FFFFFF00E7E7E700EFE7E700529C5200086B0800186B
      1800637363008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000084847B00C6C6
      C600FFFFFF00FFFFFF00FFFFFF00EFEFEF00EFEFEF00BDA5A5007B4A42007B4A
      4200AD9C9C00B5B5B500737373005A5A5A0000000000000000009C9C9400D6D6
      D600FFFFFF00FFFFFF00FFFFFF00F7F7F700F7F7EF0094B5CE0000639C000063
      9C008CADBD00C6C6C6009C9C9C007B7B7B00000000000000000084848400C6C6
      C600FFFFFF00FFFFFF00FFFFFF00EFEFEF00F7EFEF005AA55A00006B0000006B
      000084AD8400B5B5B500848484005A5A5A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084848400D6D6D600FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00F7F7F700F7F7F700BDADAD007B4A4A007B4A
      4200B59C9C00CECECE00B5B5B5005A5A5A00000000009C9C9C00E7E7E700FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00F7F7F700FFF7F70094BDCE0000639C000063
      940094B5C600DEDED600C6C6C600737373000000000084848400D6D6D600FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00F7F7F700FFF7F7005AA55A00006B0000006B
      00008CB58C00D6CECE00B5B5B5005A5A5A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084848400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00C6ADAD007B4A4A007B4A
      4200B5A5A500CED6D600B5B5B5005A5A5A00000000009C9C9C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF009CBDD60000639C000063
      940094B5C600DEDEDE00C6C6C600737373000000000084848400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0063A56300006B0000006B
      000094B59400D6D6CE00B5B5B5005A5A5A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084848400FFFFFF00FFFF
      FF00BDB5B5008C7B7B00D6D6D600FFFFFF00FFFFFF00C6ADAD00734239007342
      4200BDADA500DEDEDE00BDBDBD005A5A5A00000000009C9C9C00FFFFFF00FFFF
      FF00B5C6CE00849CAD00D6DEE700FFFFFF00FFFFFF0094BDD600005A9400005A
      940094BDCE00E7E7E700CECECE007B7B73000000000084848400FFFFFF00FFFF
      FF009CB59C00638C6300C6D6C600FFFFFF00FFFFFF0063AD6300006B0000006B
      000094BD9400DEDEDE00BDBDBD00635A5A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084848400E7E7EF007B6B
      6B00633939006B39390052393900CECECE00FFFFFF00EFE7E700C6ADAD00A584
      7B00C6BDB500DEE7E700C6C6C60052525200000000009C9C9C00EFEFEF006B8C
      A500005A8C00005A8C00185A8400CED6DE00FFFFFF00E7EFF70094BDD6005294
      B500ADC6D600EFE7E700D6D6D60052636B000000000084848400E7EFE7004A73
      4A00005200000052000008420800BDCEBD00FFFFFF00D6E7D60063AD6300318C
      3100ADCEAD00E7E7DE00CEC6CE00314A31000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000848484005A4242008C52
      5200EF949400DE8484007B42420052393900CECECE00FFFFFF00FFFFFF00F7F7
      F700EFEFEF00EFEFEF00A59C9C0052393900000000009C9C9C0029638400006B
      A5000094CE000094C60000639C00185A7B00CED6DE00FFFFFF00FFFFFF00FFFF
      F700EFEFEF00F7F7EF00CECECE0029526B000000000084848400104A1000087B
      100029D64A0021BD3900005A000008420800BDCEBD00FFFFFF00FFFFFF00F7F7
      F700EFEFEF00EFEFEF00C6C6C600103910000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000063424200C6848400FFAD
      AD00EF9C9C00E78C8C00D67B7B007B42420052313100CECECE00FFFFFF00FFFF
      FF00FFFFFF00B5ADAD00422929007342420000000000106384000084BD00009C
      D6000094CE000094CE00008CC60000639C00185A7B00CED6DE00FFFFFF00FFFF
      FF00FFFFFF00C6CED600296384000052840000000000105A100021BD420039EF
      630029D64A0021CE420018B529000052000008420800BDCEBD00FFFFFF00FFFF
      FF00FFFFFF00B5C6B50018521800003900000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000007B4A4A00FFBDBD00F7A5
      A500EF949400E78C8C00E7848400CE7373007B42420052393900D6CECE00FFFF
      FF00A594940042292900734242000000000000000000006BA500009CD600009C
      CE000094CE000094C6000094C600008CBD0000639C0018527B00CED6DE00FFFF
      FF009CADBD00084A6B00005A8C000000000000000000006308004AFF7B0031E7
      5A0029D64A0021C6420018C6310010A521000052000008420800C6D6C600FFFF
      FF0084A584000039000000420000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000007B4A4A00DE949400F7A5
      A500EF949400E78C8C00DE848400DE737300C66363007B424200524242007363
      630042292900734A4A00000000000000000000000000006BA500008CC600009C
      CE000094CE000094C6000094C600008CC6000084BD0000639C00215A7B005A7B
      8C00004A7300006394000000000000000000000000000063080031CE520039E7
      630029D64A0021C6420018BD310010B52100089C180000520000104A1000426B
      420000310000004A000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000007B4A4A00E794
      9400F79C9C00E78C8C00DE848400DE848400FFBDBD00C65A5A007B4242004A29
      2900734A4A000000000000000000000000000000000000000000006BA5000094
      CE00009CCE000094C6000094C6000894CE0018ADDE000084BD0000639C00004A
      7B0000639C0000000000000000000000000000000000000000000063080029D6
      520029DE520021C6420018BD310021C6390039E7630008940800005200000039
      0000004A00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000007B4A
      4A00DE8C8C00EF8C8C00DE848400EF949400FFBDBD00DE6B6B007B4A4A000000
      0000000000000000000000000000000000000000000000000000000000000073
      AD000094C6000094CE000094C60018ADDE0018ADDE000084BD000073A5000000
      0000000000000000000000000000000000000000000000000000000000000063
      080029C6420021CE420021C6310039E7630039E76300008C0000006300000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00007B4A4A00EF8C8C00E78484007B4A4A007B4A4A00DE6B6B007B4A4A000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000073A500008CC6000094CE000073A500008CBD000084B5000073A5000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000063080018BD310018C6310000630800007B0000006B0000006308000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000007B4A4A007B4A4A00000000007B4A4A007B4A4A00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000073A5000073A500000000000073A5000073A500000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000063080000630800000000000042000000630800000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00F8FFF8FFF8FF0000F01FF01FF01F0000
      E003E003E0030000C000C000C000000080008000800000008000800080000000
      8000800080000000800080008000000080008000800000008000800080000000
      80018001800100008003800380030000C007C007C0070000E01FE01FE01F0000
      F01FF01FF01F0000F93FF93FF93F000000000000000000000000000000000000
      000000000000}
  end
  object qry1: TADOQuery
    Connection = CommonModule.conDb
    LockType = ltBatchOptimistic
    BeforeOpen = qry1BeforeOpen
    AfterOpen = qry1AfterOpen
    Parameters = <>
    Left = 310
    Top = 142
  end
  object qry2: TADOQuery
    Connection = CommonModule.conDb
    LockType = ltBatchOptimistic
    BeforeOpen = qry1BeforeOpen
    AfterOpen = qry1AfterOpen
    Parameters = <>
    Left = 350
    Top = 142
  end
  object qry3: TADOQuery
    Connection = CommonModule.conDb
    Parameters = <>
    Left = 382
    Top = 142
  end
  object qryUPMOCTA: TADOQuery
    Connection = CommonModule.conDb
    LockType = ltBatchOptimistic
    Parameters = <>
    Left = 358
    Top = 255
  end
  object qryUPSFCTA: TADOQuery
    Connection = CommonModule.conDb
    LockType = ltBatchOptimistic
    Parameters = <>
    Left = 406
    Top = 255
  end
end
